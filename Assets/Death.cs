﻿using System;
using UnityEngine;

public class Death : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D other)
    {
        GameManager.Manager.GetComponent<GameOver>().TriggerGameOver();
    }
}
