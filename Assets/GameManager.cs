﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameObject Manager;

    private void Awake()
    {
        DontDestroyOnLoad(this);
        Manager = gameObject;
    }
}
