﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeSpawner : MonoBehaviour
{
    public GameObject Pipe;
    public float MaxTime;
    public float Timer;
    public float Height;
    
    // Start is called before the first frame update
    void Start()
    {
        var pipe = Instantiate(Pipe);
        pipe.transform.position = transform.position + new Vector3(1.5f, Random.Range(-Height, Height), 0);
        Destroy(pipe, 15);  
    }

    // Update is called once per frame
    void Update()
    {
        if (Timer > MaxTime)
        {
            var pipe = Instantiate(Pipe);
            pipe.transform.position = transform.position + new Vector3(1.5f, Random.Range(-Height, Height), 0);
            Destroy(pipe, 15); 
            Timer = 0;
        }

        Timer += Time.deltaTime;
    }
}
